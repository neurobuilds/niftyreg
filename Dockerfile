ARG BASE_DISTRO=ubuntu
ARG BASE_VERSION=20.04
ARG PACKAGE_MANAGER=apt
ARG N_BUILD_THREADS=4
ARG NIFTYREG_VERSION=
ARG NIFTYREG_COMMIT_HASH=6db8b16
ARG NIFTYREG_GIT=https://github.com/KCL-BMEIS/niftyreg.git
ARG NIFTYREG_BUILD_SHARED=ON


FROM ${BASE_DISTRO}:${BASE_VERSION} as apt-base
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update -qq && \
    apt-get install -y -q \
           libpng16-16 \
           libgomp1 && \
    rm -rf /var/lib/apt/lists/*


FROM ${BASE_DISTRO}:${BASE_VERSION} as apt-build
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update -qq && \
    apt-get install -y -q \
           cmake \
           g++ \
           gcc \
           git \
           make \
           zlib1g-dev \
           libpng-dev && \
    rm -rf /var/lib/apt/lists/*


FROM ${BASE_DISTRO}:${BASE_VERSION} as yum-base
ARG N_BUILD_THREADS

RUN yum install -y -q \
           libpng \
           libgomp && \
    yum clean packages && \
    rm -rf /var/cache/yum/* /var/tmp/*


FROM ${BASE_DISTRO}:${BASE_VERSION} as yum-build
ARG N_BUILD_THREADS

RUN yum install -y -q \
           gcc-c++ \
           git \
           make \
           zlib-devel \
           png-devel \
           openssl-devel && \
    yum clean packages && \
    rm -rf /var/cache/yum/* /var/tmp/*

RUN cd /opt && \
    curl -L --silent https://cmake.org/files/v3.20/cmake-3.20.4.tar.gz > cmake-3.20.4.tar.gz && \
    tar -xzf cmake-3.20.4.tar.gz && \
    cd cmake-3.20.4 && \
    ./bootstrap --parallel=${N_BUILD_THREADS} && \
    make -j${N_BUILD_THREADS} && \
    make install && \
    cd /opt && \
    rm -rf cmake-3.20.4


FROM ${PACKAGE_MANAGER}-build as install-niftyreg
# Install Variables
ARG N_BUILD_THREADS
ARG NIFTYREG_COMMIT_HASH
ARG NIFTYREG_GIT
ARG NIFTYREG_BUILD_SHARED

# Install NiftyReg
RUN git clone ${NIFTYREG_GIT} /tmp/niftyreg-source && \
    cd /tmp/niftyreg-source && \
    git checkout $NIFTYREG_COMMIT_HASH && \
    mkdir /tmp/niftyreg-build && \
    cd /tmp/niftyreg-build && \
    cmake /tmp/niftyreg-source \
        -DCMAKE_INSTALL_PREFIX=/opt/niftyreg \
        -DCMAKE_BUILD_TYPE=Release \
        -DBUILD_SHARED_LIBS=${NIFTYREG_BUILD_SHARED} && \
    make -j ${N_BUILD_THREADS} && \
    make install

RUN if [ -z "${NIFTYREG_VERSION}" ]; then \
        BUILD_VERSION=$(LD_LIBRARY_PATH=/opt/niftyreg/lib:$LD_LIBRARY_PATH /opt/niftyreg/bin/reg_aladin --version); \
        BUILD_HASH=${NIFTYREG_COMMIT_HASH}; \
    else \
        BUILD_VERSION=${NIFTYREG_VERSION}; \
        BUILD_HASH=$(cd /tmp/niftyreg-source;git rev-parse HEAD); \
    fi; \
    echo -e "{\
    \n  \"build_version\": \"$BUILD_VERSION\", \
    \n  \"build_commit_hash\": \"$BUILD_HASH\", \
    \n  \"build_shared_libs\": \"$NIFTYREG_BUILD_SHARED\", \
    \n  \"build_git_url\": \"$NIFTYREG_GIT\" \
    \n}" > /opt/niftyreg/manifest.json


FROM ${PACKAGE_MANAGER}-base as final
LABEL maintainer=blake.dewey@jhu.edu
# Copy Build Artifacts
COPY --from=install-niftyreg /opt/niftyreg /opt/niftyreg

# Update Environment Variables
ENV PATH /opt/niftyreg/bin:${PATH}
ENV LD_LIBRARY_PATH=/opt/niftyreg/lib:${LD_LIBRARY_PATH}

CMD ["/bin/bash"]
